#ifndef PROCESSOR_H
#define PROCESSOR_H


#include "Stack.h"
#include "Command.h"

using namespace std;


static const size_t DefaultSize = 65536; // ������ ������

class Processor
{

public:
	friend class Command;
	
	PSW psw;				//-- ������� psw
	Address addresReg;      //-- ������� ������
	Stack stack;			//-- ���� ���������� 16 ����� �� 4 �����
	Address IP;
	uMemCell memory[DefaultSize];

public:
	Processor()
	{
	};
	~Processor()
	{
	};
};

#endif // PROCESSOR_H

