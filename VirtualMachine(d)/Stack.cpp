
#include "data.h"
#include "Stack.h"
using namespace std;

bool Stack::empty()                //--�������� �� ������� 
{
	return (head == nullptr);
}

void Stack::push(uStackCell _uStackCell)            //���������� ��������
{
	if (count > 16) throw "stack overloaded";		//--����� ����� ���������� 16 ��������

	node *p = new node();
	p->data = _uStackCell;
	p->next = head;
	count++;
	head = p;
}

void Stack::pop()                    //�������� ��������
{
	if (!empty())
	{
		node* p = head;
		head = head->next;
		delete p;
		--count;
	}
	else
	{
		throw "���� ����!";
	}
}
uStackCell Stack::top()                   //�������� �������
{
	if (!empty())
		return head->data;
	else
		throw "���� ����!";
}
//--����� ����� �� ����� 
void Stack::display()
{
	int i = 0;
	node *p = this->head;
	while (!(p == nullptr))
	{
		cout<<"st["<<i<<"]" << "f - " << p->data.f << " " <<", ";
		cout << "st[" << i << "]" << "i - " << p->data.i << ",  ";
		cout << "st[" << i << "]" << "address - " << p->data.address << "\n ";
		++i;
		p = p->next;
	}
}

