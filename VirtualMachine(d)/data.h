#ifndef DATA_H
#define DATA_H

#include "Types.h"
#pragma pack(push, 1)
//-- `u` � �������� ������. �� union


//--�������
struct PSW
{
	Address IP : 16;	//--��������� �� �������
	Address SP : 4;		//--��������� �� ������� �����
	Flag END : 1;		//--���� ������������
	Flag OF : 1;		//--���� ������������
	Flag CO : 2;		//--���� ���������
};
//����� � 16 ���/2 �����
union uAddress
{
	//�������� ����� ����� � ��� �������� ��� �������
	Byte halfAddress[2];
	Address fullAddress;
};
//������� - 24 ����/3 �����
struct Cmd24
{
	Cop cop : 7;			//--��� �������
	Flag b : 1;				//--���� b:  0 � ����� (���������� ���������). 1 � ����� + �������(��������� ��� �������)
	uAddress address;	    //--�����
};
//������� - 8 ���/1 �����
struct Cmd8
{
	Cop cop : 7;			//--��� �������
	Flag b : 1;				//--���� b:  0 � ����� (���������� ���������). 1 � ����� + �������(��������� ��� �������)
};
//������ ������ - 8 ���/1 ���� 
union uMemCell
{
	Cmd8 cmd8;				//--������� 8 ���
	Byte halfAddress;		//--�������� ������ 
	Byte Data;				//--��� �������� ����� �����
};
//���������� ������ ������ � ����������� ������ ����=4 �����
union uData
{
	uMemCell memCell[4];	//4
	uAddress address[2];	//2*2 ���� 4*1

	Byte bytes[4];			//4 �����

	Float f;				//4
	Int i;					//4

	Cmd8 cmd8;				//1
	Cmd24 cmd24;			//3
};


//������ ����� - 32 ����/4 �����
union uStackCell
{
	Float f;				//--������� 4 �����
	Int i;					//--�����	4 ����� 
	uAddress address[2];	//--�����	2*2 �����
	Cmd24 cmd;				//--�������� 3 �����  
	Byte bytes[4];			//4 �����
};

#pragma pack(pop)
#endif //DATA_H