#ifndef VIRTUAL_MACHINE
#define VIRTUAL_MACHINE

//#include "data.h"
//#include "Types.h"
#include "Memory.h"
#include "processor.h"


using namespace std;


class VirtualMachine
{
public:

	Processor processor;

public:
	VirtualMachine()
	{

	};
	~VirtualMachine()
	{

	};

	void load(const string &);
	void ipLoad(const string & str, Address & CurrentPointer);
	void strParse(const string &, Address&);


	void loadAddress(stringstream &, Address&);
	void loadInt(stringstream &, Address&);
	void loadFloat(stringstream &, Address&);
	void loadCommand(stringstream &, Address &);
	void loadAddresslessCommand(stringstream &, Address &);
	void loadStop(stringstream &, Address &);
	void loadReturn(stringstream &, Address &);
	void subprogram(stringstream &, Address &);

	void ignoreComment();

	void memoryDisplay();

	
	void interpret();
};


#endif // VIRTUAL_MACHINE

