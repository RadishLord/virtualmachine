 
#include "VirtualMachine.h"
#include "CommandCodes.h"




using namespace std;

void VirtualMachine::interpret()
{
	 Command* commandArray[35];
	 commandArray[CommandCodes::NopCmd] = new Nop(); // ������ �� ������
	 commandArray[CommandCodes::StopCmd] = new Stop();// ��������� ������ ���������
			   //--����� ����������
	 commandArray[CommandCodes::AddICmd] = new AddI(); // ��������
	 commandArray[CommandCodes::SubICmd] = new SubI (); // ���������
	 commandArray[CommandCodes::MulICmd] = new MulI (); // ���������
	 commandArray[CommandCodes::DivICmd] = new DivI (); // �������
	 commandArray[CommandCodes::ModICmd] = new ModI (); // ������� �� �������
				//--������� ����������
	 commandArray[CommandCodes::AddFCmd] = new AddF (); // ��������
	 commandArray[CommandCodes::SubFCmd] = new SubF (); // ���������
	 commandArray[CommandCodes::MulFCmd] = new MulF (); // ���������
	 commandArray[CommandCodes::DivFCmd] = new DivF (); // �������

				//--���� 
	 commandArray[CommandCodes::TopFCmd] = new TopF ();   //������ �� �����(��������)
	 commandArray[CommandCodes::TopICmd] = new TopI ();   //������ �� �����(��������)
	 commandArray[CommandCodes::PushFCmd] = new PushF();  // �������� � ����
	 commandArray[CommandCodes::PushICmd] = new PushI();  // �������� � ����
	 commandArray[CommandCodes::PopCmd] = new Pop();    // �������� �������
	 commandArray[CommandCodes::ClearCmd] = new Clear();// �������� �����

				//--���������
	 commandArray[CommandCodes::CmpICmd] = new CmpI();  // ��������� �����
	 commandArray[CommandCodes::CmpFCmd] = new CmpF();  // ��������� �������

				//--���� �����
	 commandArray[CommandCodes::InICmd] = new  InI();  // ���� �����
	 commandArray[CommandCodes::OutICmd] = new OutI(); // ����� �����
	 commandArray[CommandCodes::InFCmd] = new  InF();  // ���� �������
	 commandArray[CommandCodes::OutFCmd] = new OutF(); // ����� �������

				//--��������
	 commandArray[CommandCodes::JmpCmd] = new Jmp();  // ������� � ������
	 commandArray[CommandCodes::JeCmd] = new Je();   // �������, ���� �����
	 commandArray[CommandCodes::JneCmd] = new Jne();  // �������, ���� �� �����
	 commandArray[CommandCodes::JlCmd] = new Jl();   // �������, ���� ������
	 commandArray[CommandCodes::JgCmd] = new Jg();   // �������, ���� ������
	 commandArray[CommandCodes::JleCmd] = new Jle();  // �������, ���� ������ ��� �����
	 commandArray[CommandCodes::JgeCmd] = new Jge();  // �������, ���� ������ ��� �����
	 commandArray[CommandCodes::CallCmd] = new Call(); // ����� ������������
	 commandArray[CommandCodes::RetCmd] = new Ret();  // ����������� �� ������������
	 commandArray[CommandCodes::AddrLoadCmd] = new AddrLoad();  //�������� � �������� �������

	 uAddress _uAddress;
	 int i = 0;
	 while (processor.psw.END != 1)
	 {
		 Cmd8 currCmd8;
		 if (processor.IP >= 0 && processor.IP <= 65535)
		 {
			 currCmd8.cop = processor.memory[processor.IP].cmd8.cop;
			 currCmd8.b = processor.memory[processor.IP].cmd8.b;
		 }
		 else
			 throw "IP error" + processor.IP;


		 if (currCmd8.b == 1)  //--��������� ���������. ����� + �������
		 {
			 //��������� ����� �� ������
			 uAddress curAddress;

			 curAddress.halfAddress[0] = processor.memory[processor.IP + 1].halfAddress;
			 curAddress.halfAddress[1] = processor.memory[processor.IP + 2].halfAddress;

			 //���������� � ���� �������� �� �������� ������
			 curAddress.fullAddress += processor.addresReg;
			 //������ �� �����
			 processor.memory[processor.IP + 1].halfAddress = curAddress.halfAddress[0];
			 processor.memory[processor.IP + 2].halfAddress = curAddress.halfAddress[0];
		 }

		 

		 if (currCmd8.cop >= 0 && currCmd8.cop <= 32)
		 {
			 try
			 {
				 //cout << "\n____________\n" << "i: " << i << "  ip:" << processor.IP<<"  cop: "<< currCmd8.cop <<"\n";
				 commandArray[currCmd8.cop]->operator()(processor);
				 ++i;
			 }
			 catch(char s[])
			 {
				 cout << s << endl;
			 }
	
		 }
		 else
			 throw "\n�������� ��� ��������!\n";
		 ++processor.IP;
		 //system("pause");
	 }
}
