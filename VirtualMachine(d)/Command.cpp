#include "Command.h"


void Command::operator ()(Processor &PR)
{
	execute(PR);
}

void Nop::execute(Processor &proc)
{
	//--��� �����
	//cout << "C������� nop; ip=" << proc.IP << endl;
}
void Stop::execute(Processor &proc)
{
	//cout << "C������� stop; ip=" << proc.IP << endl;
	proc.psw.END = 1;
}
		   //--����� ����������+
void AddI::execute(Processor &proc)
{
	
	//���������� ��� ������� �������� � �����. ��������� � ����
	uStackCell curCell1, curCell2;
	try
	{
		curCell1 = proc.stack.top();
		proc.stack.pop();
		curCell2 = proc.stack.top();
		proc.stack.pop();
	}
	catch (string s)
	{
		throw s;
	}


	uStackCell res;
	res.i = curCell1.i + curCell2.i;
	//cout << "C������� AddI; ip=" << proc.IP <<"res = " << res.i << " (" << curCell1.i << "+" <<curCell2.i << endl;
	proc.stack.push(res);
} 
void SubI::execute(Processor &proc)
{
	//cout << "C������� SubI; ip=" << proc.IP << endl;
	//�������� ������ ������� ����� �� �������. ��������� � ����
	uStackCell curCell1, curCell2;
	try
	{
		curCell1 = proc.stack.top();
		proc.stack.pop();
		curCell2 = proc.stack.top();
		proc.stack.pop();
	}
	catch (string s)
	{
		throw s;
	}


	uStackCell res;
	res.i = curCell1.i - curCell1.i;
	proc.stack.push(res);
}
void MulI::execute(Processor &proc)
{
	//cout << "C������� MulI; ip=" << proc.IP << endl;
	//�������� ������ ������� ����� �� ������. ��������� � ����
	uStackCell curCell1, curCell2;
	try
	{
		curCell1 = proc.stack.top();
		proc.stack.pop();
		curCell2 = proc.stack.top();
		proc.stack.pop();
	}
	catch (string s)
	{
		throw s;
	}

	uStackCell res;
	res.i = curCell1.i * curCell2.i;
	proc.stack.push(res);
}
void DivI::execute(Processor &proc)
{
	//cout << "C������� DivI; ip=" << proc.IP << endl;
	//����� ������ ������� ����� �� ������. ��������� � ����
	uStackCell curCell1, curCell2;
	try
	{
		curCell1 = proc.stack.top();
		proc.stack.pop();
		curCell2 = proc.stack.top();
		proc.stack.pop();
	}
	catch(string s)
	{
		throw s;
	}


	uStackCell res;
	res.i = curCell2.i / curCell1.i;
	proc.stack.push(res);
}
void ModI::execute(Processor &proc)
{
	//cout << "C������� ModI; ip=" << proc.IP << endl;
	//����� ������ ������� ����� �� ������. ������� � ����
	uStackCell curCell1, curCell2;
	try
	{
		curCell1 = proc.stack.top();
		proc.stack.pop();
		curCell2 = proc.stack.top();
		proc.stack.pop();
	}
	catch (string s)
	{
		throw s;
	}


	uStackCell res;
	res.i = curCell2.i % curCell1.i;
	proc.stack.push(res);
}

			//--������� ����������+
void AddF::execute(Processor &proc)
{
	//cout << "C������� AddF; ip=" << proc.IP << endl;
	//���������� ��� ������� �������� � �����. ��������� � ����
	uStackCell curCell1, curCell2;
	try
	{
		curCell1 = proc.stack.top();
		proc.stack.pop();
		curCell2 = proc.stack.top();
		proc.stack.pop();
	}
	catch (string s)
	{
		throw s;
	}


	uStackCell res;
	res.f = curCell1.f + curCell2.f;
	proc.stack.push(res);
}
void SubF::execute(Processor &proc)
{
	//cout << "C������� SubF; ip=" << proc.IP << endl;
	//�������� ������ ������� ����� �� �������. ��������� � ����
	uStackCell curCell1, curCell2;
	try
	{
		curCell1 = proc.stack.top();
		proc.stack.pop();
		curCell2 = proc.stack.top();
		proc.stack.pop();
	}
	catch (string s)
	{
		throw s;
	}


	uStackCell res;
	res.f = curCell2.f - curCell1.f;
	proc.stack.push(res);
}
void MulF::execute(Processor &proc)
{
	//cout << "C������� MulF; ip=" << proc.IP << endl;
	//�������� ������ ������� ����� �� ������. ��������� � ����
	uStackCell curCell1, curCell2;
	try
	{
		curCell1 = proc.stack.top();
		proc.stack.pop();
		curCell2 = proc.stack.top();
		proc.stack.pop();
	}
	catch (string s)
	{
		throw s;
	}


	uStackCell res;
	res.f = curCell1.f * curCell2.f;
	proc.stack.push(res);
}
void DivF::execute(Processor &proc)
{
	//cout << "C������� DivF; ip=" << proc.IP << endl;
	//����� ������ ������� ����� �� ������. ��������� � ����
	uStackCell curCell1, curCell2;
	try
	{
		curCell1 = proc.stack.top();
		proc.stack.pop();
		curCell2 = proc.stack.top();
		proc.stack.pop();
	}
	catch (string s)
	{
		throw s;
	}


	uStackCell res;
	res.f = curCell2.f / curCell1.f;
	proc.stack.push(res);
}

			//--����+
void TopF::execute(Processor &proc)
{
	//cout << "C������� TopF; ip=" << proc.IP << endl;

	//��������� ����� �� ������ 
	uAddress _curAddress;

	_curAddress.halfAddress[0] = proc.memory[++proc.IP].halfAddress;
	_curAddress.halfAddress[1] = proc.memory[++proc.IP].halfAddress;

	//�������� ������������ ������� �� �����
	uStackCell curCell;
	curCell = proc.stack.top();

	//���������� ����� � ����� Data ��� ����������� �� 4 ������
	uData curData;
	curData.f = curCell.f;

	//���������� ����� � ������ �� ���������� ������ (� 3 ���������)
	proc.memory[_curAddress.fullAddress].Data = curData.bytes[0];
	proc.memory[++_curAddress.fullAddress].Data = curData.bytes[1];
	proc.memory[++_curAddress.fullAddress].Data = curData.bytes[2];
	proc.memory[++_curAddress.fullAddress].Data = curData.bytes[3];

	//cout << "stack.Top.f:" << curCell.f << endl;

}  
void TopI::execute(Processor &proc)
{
	//cout << "C������� TopI; ip=" << proc.IP << endl;

	//��������� ����� �� ������ 
	uAddress _curAddress;

	_curAddress.halfAddress[0] = proc.memory[++proc.IP].halfAddress;
	_curAddress.halfAddress[1] = proc.memory[++proc.IP].halfAddress;

	//�������� ������������ ������� �� �����
	uStackCell curCell;
	curCell = proc.stack.top();

	//���������� ����� � ����� Data ��� ����������� �� 4 ������
	uData curData;
	curData.i = curCell.i;

	//���������� ����� � ������ �� ���������� ������ (� 3 ���������)
	proc.memory[_curAddress.fullAddress].Data = curData.bytes[0];
	proc.memory[++_curAddress.fullAddress].Data = curData.bytes[1];
	proc.memory[++_curAddress.fullAddress].Data = curData.bytes[2];
	proc.memory[++_curAddress.fullAddress].Data = curData.bytes[3];

	//cout << "stack.Top.i:" << curCell.i << endl;
}
void PushF::execute(Processor &proc)
{
	//cout << "C������� PushF; ip=" << proc.IP << endl;

	//��������� ����� �� ������ 
	uAddress _curAddress;

	_curAddress.halfAddress[0] = proc.memory[++proc.IP].halfAddress;
	_curAddress.halfAddress[1] = proc.memory[++proc.IP].halfAddress;

	//�������� ������������ ������� �� ������ � ����� Data ��� �������
	uData curData;

	curData.bytes[0] = proc.memory[_curAddress.fullAddress].Data;
	curData.bytes[1] = proc.memory[++_curAddress.fullAddress].Data;
	curData.bytes[2] = proc.memory[++_curAddress.fullAddress].Data;
	curData.bytes[3] = proc.memory[++_curAddress.fullAddress].Data;


	//���������� ���������� ����� � ����� �����
	uStackCell curStackCell;
	curStackCell.f = curData.f;

	//����� ����� � ����
	proc.stack.push(curStackCell);

	//cout << "stack.Top.f:" << curStackCell.f << endl;

} 
void PushI::execute(Processor &proc)
{
	//cout << "C������� PushI; ip=" << proc.IP << endl;

	//��������� ����� �� ������ 
	uAddress _curAddress;

	_curAddress.halfAddress[0] = proc.memory[++proc.IP].halfAddress;
	_curAddress.halfAddress[1] = proc.memory[++proc.IP].halfAddress;

	//�������� ������������ ������� �� ������ � ����� Data ��� �������
	uData curData;

	curData.bytes[0] = proc.memory[_curAddress.fullAddress].Data;
	curData.bytes[1] = proc.memory[++_curAddress.fullAddress].Data;
	curData.bytes[2] = proc.memory[++_curAddress.fullAddress].Data;
	curData.bytes[3] = proc.memory[++_curAddress.fullAddress].Data;


	//���������� ���������� ����� � ����� �����
	uStackCell curStackCell;
	curStackCell.i = curData.i;

	//����� ����� � ����
	proc.stack.push(curStackCell);

	//cout << "stack.Top.i:" << curStackCell.i << endl;
}
void Pop::execute(Processor &proc)
{
	//cout << "C������� Pop; ip=" << proc.IP << endl;
	proc.stack.pop();
}  
void Clear::execute(Processor &proc)
{
	//cout << "C������� Clear; ip=" << proc.IP << endl;
	while(!proc.stack.empty())
	{
		proc.stack.pop();
	}
}

			//--���������
void CmpI::execute(Processor &proc)
{
	//cout << "C������� CmpI; ip=" << proc.IP << endl;

	//��������� ����� �� ������ 
	uAddress _curAddress;

	_curAddress.halfAddress[0] = proc.memory[++proc.IP].halfAddress;
	_curAddress.halfAddress[1] = proc.memory[++proc.IP].halfAddress;

	//�������� ����� ������� �� ������ � ����� Data ��� �������
	uData curData;

	curData.bytes[0] = proc.memory[_curAddress.fullAddress].Data;
	curData.bytes[1] = proc.memory[++_curAddress.fullAddress].Data;
	curData.bytes[2] = proc.memory[++_curAddress.fullAddress].Data;
	curData.bytes[3] = proc.memory[++_curAddress.fullAddress].Data;

	uStackCell stackCell;
	stackCell = proc.stack.top();

	uData nextData;

	
	if (stackCell.i == curData.i) proc.psw.CO = 0;
	else if (stackCell.i > curData.i) proc.psw.CO = 1;
	else if (stackCell.i < curData.i) proc.psw.CO = 2;
	//cout << "cmpI: " << stackCell.i <<" --- " << curData.i << " co:" << proc.psw.CO<<endl ;
}
void CmpF::execute(Processor &proc)
{
	//cout << "C������� CmpF; ip=" << proc.IP << endl;

	//��������� ����� �� ������ 
	uAddress _curAddress;

	_curAddress.halfAddress[0] = proc.memory[++proc.IP].halfAddress;
	_curAddress.halfAddress[1] = proc.memory[++proc.IP].halfAddress;

	//�������� ����� ������� �� ������ � ����� Data ��� �������
	uData curData;

	curData.bytes[0] = proc.memory[_curAddress.fullAddress].Data;
	curData.bytes[1] = proc.memory[++_curAddress.fullAddress].Data;
	curData.bytes[2] = proc.memory[++_curAddress.fullAddress].Data;
	curData.bytes[3] = proc.memory[++_curAddress.fullAddress].Data;

	uStackCell stackCell;
	stackCell = proc.stack.top();

	uData nextData;

	if (stackCell.f == curData.f) proc.psw.CO = 0;
	else if (stackCell.f > curData.f) proc.psw.CO = 1;
	else if (stackCell.f < curData.f) proc.psw.CO = 2;
}

			//--���� �����+
void InI::execute(Processor &proc)
{
	//��������n ����� �� ������ 
	uAddress _curAddress;

	_curAddress.halfAddress[0] = proc.memory[++proc.IP].halfAddress;
	_curAddress.halfAddress[1] = proc.memory[++proc.IP].halfAddress;

	//������� � ����� int 
	uData cell;
	std::cout << "������� ����� �����: " << std::endl;
	std::cin >> cell.i;

	//���������� � ��������� � ������
	proc.memory[_curAddress.fullAddress].Data = cell.bytes[0];
	proc.memory[++_curAddress.fullAddress].Data = cell.bytes[1];
	proc.memory[++_curAddress.fullAddress].Data = cell.bytes[2];
	proc.memory[++_curAddress.fullAddress].Data = cell.bytes[3];
} 
void OutI::execute(Processor &proc)
{
	//��������� ����� �� ������ 
	uAddress _curAddress;

	_curAddress.halfAddress[0] = proc.memory[++proc.IP].halfAddress;
	_curAddress.halfAddress[1] = proc.memory[++proc.IP].halfAddress;


	//��������� ������ � ����� Data ��� ������� � ������
	uData data;
	data.bytes[0] = proc.memory[_curAddress.fullAddress].Data;
	data.bytes[1] = proc.memory[++_curAddress.fullAddress].Data;
	data.bytes[2] = proc.memory[++_curAddress.fullAddress].Data;
	data.bytes[3] = proc.memory[++_curAddress.fullAddress].Data;

	//cout << "memory.i[" << _curAddress.fullAddress - 3 << "]: ";
	cout << data.i <<endl;
	
} 
void InF::execute(Processor &proc)
{
	//��������n ����� �� ������ 
	uAddress _curAddress;

	_curAddress.halfAddress[0] = proc.memory[++proc.IP].halfAddress;
	_curAddress.halfAddress[1] = proc.memory[++proc.IP].halfAddress;

	//������� � ����� ������������ 
	uData cell;
	std::cout << "������� ������������ �����: " << std::endl;
	std::cin >> cell.f;

	//���������� � ��������� � ������
	proc.memory[_curAddress.fullAddress].Data = cell.bytes[0];
	proc.memory[++_curAddress.fullAddress].Data = cell.bytes[1];
	proc.memory[++_curAddress.fullAddress].Data = cell.bytes[2];
	proc.memory[++_curAddress.fullAddress].Data = cell.bytes[3];
}  
void OutF::execute(Processor &proc)
{
	//��������� ����� �� ������ 
	uAddress _curAddress;

	_curAddress.halfAddress[0] = proc.memory[++proc.IP].halfAddress;
	_curAddress.halfAddress[1] = proc.memory[++proc.IP].halfAddress;


	//��������� ������ � ����� Data ��� ������� � ������
	uData data;
	data.bytes[0] = proc.memory[_curAddress.fullAddress].Data;
	data.bytes[1] = proc.memory[++_curAddress.fullAddress].Data;
	data.bytes[2] = proc.memory[++_curAddress.fullAddress].Data;
	data.bytes[3] = proc.memory[++_curAddress.fullAddress].Data;

	//cout << "memory.f[" << _curAddress.fullAddress - 3 << "]: " << endl;
	cout << data.f << endl;
}

			//--��������
void Jmp::execute(Processor &proc)
{
	//��������� ����� �� ������ 
	uAddress _curAddress;

	_curAddress.halfAddress[0] = proc.memory[++proc.IP].halfAddress;
	_curAddress.halfAddress[1] = proc.memory[++proc.IP].halfAddress;

	//��������� ���
	if ((_curAddress.fullAddress < 0) || (_curAddress.fullAddress > 65536))
		throw "Incorrect address. IP = " + proc.IP;

	//������ ��� � IP
	proc.IP = _curAddress.fullAddress - 1;
}
void Je::execute(Processor &proc)
{
	//�������, ���� �����
	//��������� ����� �� ������ 
	uAddress _curAddress;

	_curAddress.halfAddress[0] = proc.memory[++proc.IP].halfAddress;
	_curAddress.halfAddress[1] = proc.memory[++proc.IP].halfAddress;

	//��������� ���
	if ((_curAddress.fullAddress < 0) || (_curAddress.fullAddress > 65536))
		throw "Incorrect address. IP = " + proc.IP;
	
	//���� � ��������� ��������� = 0 �� ������ ����� � IP
	if (proc.psw.CO == 0)
		proc.IP = _curAddress.fullAddress - 1;
}
void Jne::execute(Processor &proc)
{
	//�������, ���� �� �����
	//��������� ����� �� ������ 
	uAddress _curAddress;

	_curAddress.halfAddress[0] = proc.memory[++proc.IP].halfAddress;
	_curAddress.halfAddress[1] = proc.memory[++proc.IP].halfAddress;

	//��������� ���
	if ((_curAddress.fullAddress < 0) || (_curAddress.fullAddress > 65536))
		throw "Incorrect address. IP = " + proc.IP;

	//���� � ��������� ��������� != 0 �� ������ ����� � IP
	if (proc.psw.CO != 0)
		proc.IP = _curAddress.fullAddress - 1;
}
void Jl::execute(Processor &proc)
{
	//�������, ���� ������
	//��������� ����� �� ������ 
	uAddress _curAddress;

	_curAddress.halfAddress[0] = proc.memory[++proc.IP].halfAddress;
	_curAddress.halfAddress[1] = proc.memory[++proc.IP].halfAddress;

	//��������� ���
	if ((_curAddress.fullAddress < 0) || (_curAddress.fullAddress > 65536))
		throw "Incorrect address. IP = " + proc.IP;


	//���� � ��������� ��������� = 0 �� ������ ����� � IP
	if (proc.psw.CO == 2)
	{
		proc.IP = _curAddress.fullAddress - 1;
	}

}
void Jg::execute(Processor &proc)
{
	//�������, ���� ������
	//��������� ����� �� ������ 
	uAddress _curAddress;

	_curAddress.halfAddress[0] = proc.memory[++proc.IP].halfAddress;
	_curAddress.halfAddress[1] = proc.memory[++proc.IP].halfAddress;

	//��������� ���
	if ((_curAddress.fullAddress < 0) || (_curAddress.fullAddress > 65536))
		throw "Incorrect address. IP = " + proc.IP;

	//���� � ��������� ��������� = 1 �� ������ ����� � IP
	if (proc.psw.CO == 1)
		proc.IP = _curAddress.fullAddress - 1;
}
void Jle::execute(Processor &proc)
{
	//�������, ���� ������ ��� �����
	//��������� ����� �� ������ 
	uAddress _curAddress;

	_curAddress.halfAddress[0] = proc.memory[++proc.IP].halfAddress;
	_curAddress.halfAddress[1] = proc.memory[++proc.IP].halfAddress;

	//��������� ���
	if ((_curAddress.fullAddress < 0) || (_curAddress.fullAddress > 65536))
		throw "Incorrect address. IP = " + proc.IP;

	//���� � ��������� ��������� = 0 �� ������ ����� � IP
	if (proc.psw.CO == 2 || proc.psw.CO == 0)
		proc.IP = _curAddress.fullAddress - 1;
}
void Jge::execute(Processor &proc)
{
	//�������, ���� ������ ��� �����
	//��������� ����� �� ������ 
	uAddress _curAddress;

	_curAddress.halfAddress[0] = proc.memory[++proc.IP].halfAddress;
	_curAddress.halfAddress[1] = proc.memory[++proc.IP].halfAddress;

	//��������� ���
	if ((_curAddress.fullAddress < 0) || (_curAddress.fullAddress > 65536))
		throw "Incorrect address. IP = " + proc.IP;

	//���� � ��������� ��������� = 0 �� ������ ����� � IP
	if (proc.psw.CO == 1 || proc.psw.CO == 0)
		proc.IP = _curAddress.fullAddress - 1;
}
void Call::execute(Processor &proc)
{
	//��������� ������� ����� � ������� �������
	proc.addresReg = proc.IP;

	//��������n ����� �� ������ 
	uAddress _curAddress;

	_curAddress.halfAddress[0] = proc.memory[++proc.IP].halfAddress;
	_curAddress.halfAddress[1] = proc.memory[++proc.IP].halfAddress;


	//��������� ���
	if ((_curAddress.fullAddress < 0) || (_curAddress.fullAddress > 65536))
		throw "Incorrect address. IP = " + proc.IP;

	//������ ��� � IP
	proc.IP = _curAddress.fullAddress - 1;
	
}
void Ret::execute(Processor &proc)
{

	//��������� ����� �������� �� ������� �������
	 proc.IP = proc.addresReg ;
	 //cout << "\nret" << proc.IP << " == " << proc.addresReg;
}

			//--�������� � �������� �������
void AddrLoad::execute(Processor &proc)
{
	//��������n ����� �� ������ 
	uAddress _curAddress;

	_curAddress.halfAddress[0] = proc.memory[++proc.IP].halfAddress;
	_curAddress.halfAddress[1] = proc.memory[++proc.IP].halfAddress;

	//��������� ���������� ����� � ������� �������
	proc.addresReg = _curAddress.fullAddress;
}

