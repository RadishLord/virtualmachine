#include "VirtualMachine.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include "CommandCodes.h"

using namespace std;
//	��������� ��������� � ������ vm  
void VirtualMachine::load(const string &fileName)
{
	//--��������� ���� � ����������
	ifstream file(fileName);
	string str;
	Address CurrentPointer;	//--������� �����

	//int counter;
	if (!file.is_open())
		throw "FileError";

	//--��������� IP ����������
	getline(file, str);//��������� ������ �� �����
	ipLoad(str, CurrentPointer);

	//--���� �� ����� �����, ������������ ���� � ����������
	while (!file.eof())
	{
		getline(file, str);
		//cout << str << ")\n";
		strParse(str, CurrentPointer);	
	}
}
//��������� � ��������� IP � ���������, ����. ������� ���������
void VirtualMachine::ipLoad(const string &str, Address &CurrentPointer)
{
	stringstream ss(str);
	string subStr;

	ss >> subStr;
	if (subStr == "IP")
	{
		ss >> subStr;
		processor.IP = stoi(subStr);
		CurrentPointer = stoi(subStr);
	}
	else
		processor.IP = 1;
}
//��������� ������+
void VirtualMachine::strParse(const string &str, Address &CurrentPointer)
{
	stringstream ss(str);
	string subStr;

	ss >> subStr;
	//--��������� �� ���� ������
	if (subStr == "i") loadInt(ss, CurrentPointer);							//����� ��������� - � ����
	else if (subStr == "f") loadFloat(ss, CurrentPointer);					//������������ ��������� - � ����
	else if (subStr == "ak") loadAddresslessCommand(ss, CurrentPointer);	//����������� ������� 
	else if (subStr == "k") loadCommand(ss, CurrentPointer);				//������� � �������
	else if (subStr == "e") loadStop(ss, CurrentPointer);					//����� ���������
	else if (subStr == "//") ignoreComment();								//�����������
	else if (subStr == "sp") subprogram(ss, CurrentPointer);
}
//�������� ������ � ������+
void VirtualMachine::loadAddress(stringstream &ss, Address&CurrentPointer)
{
	uAddress _address;
	ss >> _address.fullAddress;//��������� �������
	processor.memory[++CurrentPointer].halfAddress = _address.halfAddress[0];
	processor.memory[++CurrentPointer].halfAddress = _address.halfAddress[1];


}
//�������� ������ � ����+
void VirtualMachine::loadInt(stringstream &ss, Address&CurrentPointer)
{
	//��������� �� ��������� �����
	string subStr;
	ss >> subStr;

	//������ � ����� �����
	uStackCell data;
	data.i = stoi(subStr);
	processor.stack.push(data);
}
//�������� �������� � ����+
void VirtualMachine::loadFloat( stringstream &ss, Address&CurrentPointer)
{
	//��������� �� ��������� �������
	string subStr;
	ss >> subStr;

	//������ � ����� �����
	uStackCell data;
	data.f = stof(subStr);
	processor.stack.push(data);
}
//�������� ������������ ������� � ������+
void VirtualMachine::loadCommand(stringstream &ss, Address&CurrentPointer)
{
	// ����� ��������� �������={���, b, �����}

	Cop _cop;
	Flag _b;
	uAddress _address;
	ss >> _cop;
	ss >> _b;

	Cmd8 currentCmd8;
	currentCmd8.cop = _cop;
	currentCmd8.b = _b;

	uMemCell curMemCell;
	curMemCell.cmd8 = currentCmd8;
	processor.memory[CurrentPointer] = curMemCell;
	++CurrentPointer;

	ss >> _address.fullAddress;	

	//����� ����� ������� � ������ � ��� ������
	processor.memory[CurrentPointer].halfAddress = _address.halfAddress[0];
	++CurrentPointer;
	processor.memory[CurrentPointer].halfAddress = _address.halfAddress[1];
	++CurrentPointer;


}
//�������� ����������� ������� � ������
void VirtualMachine::loadAddresslessCommand(stringstream &ss, Address &CurrentPointer)
{
	// ����� ��������� �������={���, b}

	Cop _cop;
	Flag _b;
	ss >> _cop;
	ss >> _b;

	Cmd8 currentCmd8;
	currentCmd8.cop = _cop;
	currentCmd8.b = _b;

	uMemCell curMemCell;
	curMemCell.cmd8 = currentCmd8;
	processor.memory[CurrentPointer] = curMemCell;
	++CurrentPointer;


}
//--������ ������� ��������� � ������
void VirtualMachine::loadStop(stringstream &ss, Address &CurrentPointer)
{
	
	processor.memory[CurrentPointer].cmd8.cop = CommandCodes::StopCmd;
	//cout << "loadStop"<< CurrentPointer << endl;

}
void VirtualMachine::loadReturn(stringstream &ss, Address &CurrentPointer)
{
	processor.memory[CurrentPointer].cmd8.cop = CommandCodes::RetCmd;
}
void VirtualMachine::ignoreComment()
{
	//���������� �������
}
//--�������� ������������ � ������
void VirtualMachine::subprogram(stringstream &ss, Address &CurrentPointer)
{
	uAddress _address;
	ss >> _address.fullAddress;//��������� �������
	CurrentPointer = _address.fullAddress;
}
//--������� �� ����� ���������� ������, ������� � IP
void VirtualMachine::memoryDisplay()
{
	Address CurAddress = processor.IP;

	cout << "���������� ������:\n";
	//while (processor.memory[CurAddress].cmd8.cop != 1)
	while (CurAddress != 100)
	{
		cout << "[" << CurAddress << "] cop - " << processor.memory[CurAddress].cmd8.cop << endl;
		cout << "[" << CurAddress << "] b - " << processor.memory[CurAddress].cmd8.b << endl;
		cout << "[" << CurAddress << "] hAddress - " << processor.memory[CurAddress].halfAddress << endl;
		cout << "_________________________________________________" << endl;
		++CurAddress;
	}


	cout << "���������� c����:\n";
	processor.stack.display();
}
