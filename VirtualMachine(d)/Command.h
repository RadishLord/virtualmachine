#ifndef COMMAND
#define COMMAND


#include "Processor.h"

class Nop; // ������ �� ������
class Stop;// ��������� ������ ���������

//--����� ����������
class AddI; // ��������
class SubI; // ���������
class MulI; // ���������
class DivI; // �������
class ModI; // ������� �� �������

//--������� ����������
class AddF; // ��������
class SubF; // ���������
class MulF; // ���������
class DivF; // �������
 
//--����
class TopF;  //������ �� �����(��������) ������������
class TopI;  //������ �� �����(��������) �����
class PushF; // �������� � ����
class PushI; // �������� � ����
class Pop;  // �������� �������
class Clear;// �������� �����

//--���������
class CmpI; // ��������� �����
class CmpF; // ��������� �������

//--���� �����
class InI;  // ���� �����
class OutI; // ����� �����
class InF;  // ���� �������
class OutF; // ����� �������

//--��������
class Jmp;  // ������� � ������
class Je;   // �������, ���� �����
class Jne;  // �������, ���� �� �����
class Jl;   // �������, ���� ������
class Jg;   // �������, ���� ������
class Jle;  // �������, ���� ������ ��� �����
class Jge;  // �������, ���� ������ ��� �����
class Call; // ����� ������������
class Ret;  // ����������� �� ������������

class AddrLoad; //�������� ��������� ��������

//--------------------------------------------------------------------//

//--����������� ����� Command 
class Command
{
public:
	friend class Processor;
	void operator()(Processor & PR);
	virtual void execute(Processor & PR) = 0;

};



class Nop : public Command
{
public:
	virtual void execute(Processor & );
}; 
class Stop : public Command
{
public:
	virtual void execute(Processor & );
};


class AddI : public Command
{
public:
	virtual void execute(Processor & );
};
class SubI : public Command
{
public:
	virtual void execute(Processor & );
};
class MulI : public Command
{
public:
	virtual void execute(Processor & );
};
class DivI : public Command
{
public:
	virtual void execute(Processor & );
};
class ModI : public Command
{
public:
	virtual void execute(Processor & );
};
//--������� ����������
class AddF : public Command
{
public:
	virtual void execute(Processor & );
};
class SubF : public Command
{
public:
	virtual void execute(Processor & );
};
class MulF : public Command
{
public:
	virtual void execute(Processor & );
};
class DivF : public Command
{
public:
	void execute(Processor & );
};
//--����
class TopF : public Command
{
public:
	virtual void execute(Processor & );
};
class TopI : public Command
{
public:
	virtual void execute(Processor & );
};
class PushF : public Command
{
public:
	virtual void execute(Processor & );
};
class PushI : public Command
{
public:
	virtual void execute(Processor &);
};
class Pop : public Command
{
public:
	virtual void execute(Processor & );
};
class Clear : public Command
{
public:
	virtual void execute(Processor & );
};
//--���������
class CmpI : public Command
{
public:
	virtual void execute(Processor & );
};
class CmpUI : public Command
{
public:
	virtual void execute(Processor & );
};
class CmpF : public Command
{
public:
	virtual void execute(Processor & );
};
//--���� �����
class InI : public Command
{
public:
	virtual void execute(Processor & );
};
class OutI : public Command
{
public:
	virtual void execute(Processor & );
};
class InF : public Command
{
public:
	virtual void execute(Processor & );
};
class OutF : public Command
{
public:
	virtual void execute(Processor & );
};



//--��������
class Jmp : public Command
{
public:

	virtual void execute(Processor & proc);
};
class Je : public Command
{
public:

	virtual void execute(Processor & proc);
};
class Jne : public Command
{
public:
	virtual void execute(Processor & proc);
};
class Jl : public Command
{
public:
	virtual void execute(Processor & proc);
};
class Jg : public Command
{
public:
	virtual void execute(Processor & proc);
};
class Jle : public Command
{
public:
	virtual void execute(Processor & proc);
};
class Jge : public Command
{
public:
	virtual void execute(Processor & proc);
};
class Call : public Command
{
public:
	virtual void execute(Processor & proc);
};
class Ret : public Command
{
public:
	virtual void execute(Processor & proc);
};

class AddrLoad : public Command
{
public:
	virtual void execute(Processor &);
};
#endif	//COMMAND