#ifndef STACK_H
#define STACK_H

#include <iostream>         
#include "Data.h"
//--���� �� ������

//������� ������
struct node                     
{
	uStackCell data;
	node *next;
};
//���� ���������� 16 ����� �� 4 ����� 
struct Stack
{
private:
	node *head;					//--��������� �� ������
	int count;					//--������� ���������
public:
	Stack()						//--�����������
	{
		head = nullptr;
		count = 0;
	}
	bool empty();						//--�������� �� ������� 
	void push(uStackCell);				//--���������� ��������
	void pop();							//--�������� ��������
	uStackCell top();                   //--�������� �������
	void display();						//--����� ����� �� �����(!)
};


#endif // STACK_H